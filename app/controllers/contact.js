var winprocs = require('winprocs');
var procs = require('procs');

var args = arguments[0] || {};

var d = Alloy.Globals.data.pages[args.id];
var items = d.items;

$.contCont.setWidth(winprocs.screenWidth);

//Add top pad
$.contCont.add(Ti.UI.createView({
	width: winprocs.screenWidth - 40,
	height: 10
}));

for(var i in items){
	if(items[i].type == "phone"){
		var btn = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: 44,
			top: 5,
			left: 20,
			right: 20,
			bottom: 5,
			backgroundColor: Alloy.Globals.data.globalVals.palette.primary,
			data: {type: 'button', linkTo: 'phone', link: items[i].number}
		});
		
		var label = Ti.UI.createLabel({
			height: Ti.UI.SIZE,
			left: 10,
			right: 10,
			textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
			color: Alloy.Globals.data.globalVals.palette.background,
			font: {fontSize: 22},
			text: items[i].number,
			touchEnabled: false
		});
		
		btn.add(label);
		Alloy.Globals.fa.add(label, 'fa-phone');
		$.contCont.add(btn);
	} else if(items[i].type == "email"){
		var btn = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: 44,
			top: 5,
			left: 20,
			right: 20,
			bottom: 5,
			backgroundColor: Alloy.Globals.data.globalVals.palette.primary,
			data: {type: 'button', linkTo: 'email', link: items[i].address}
		});
		
		var label = Ti.UI.createLabel({
			left: 10,
			right: 10,
			textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
			color: Alloy.Globals.data.globalVals.palette.background,
			font: {fontSize: 22},
			text: items[i].address,
			touchEnabled: false
		});
		
		btn.add(label);
		Alloy.Globals.fa.add(label, 'fa-envelope-o');
		$.contCont.add(btn);
	}
}
