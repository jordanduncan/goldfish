var winprocs = require('winprocs');
var procs = require('procs');

var args = arguments[0] || {};

var d = Alloy.Globals.data;
var items = d.pages[args.id].items;

$.textCont.setWidth(winprocs.screenWidth);

//Add top pad
$.textCont.add(Ti.UI.createView({
	width: winprocs.screenWidth - 40,
	height: 10
}));

//Add items
for(var i in items){
	if(items[i].type == 'image'){
		var img = Ti.UI.createImageView({
			image: items[i].src,
			width: winprocs.screenWidth - 40,
			height: (winprocs.screenWidth - 40) * items[i].hwRatio,
			top: 10,
			left: 20,
			right: 20,
			bottom: 10
		});
		if(OS_ANDROID) procs.loadRemoteImage(img,items[i].src);
		$.textCont.add(img);
	} else if(items[i].type == 'heading'){
		var heading = Ti.UI.createLabel({
			text: items[i].text,
			width: Ti.UI.FILL,
			height: Ti.UI.SIZE,
			top: 5,
			left: 20,
			right: 20,
			bottom: 5,
			color: d.globalVals.palette.textPrimary,
			font:{fontSize: 24}
		});
		$.textCont.add(heading);
	} else if(items[i].type == 'text'){
		var text = Ti.UI.createLabel({
			text: items[i].text,
			width: Ti.UI.FILL,
			height: Ti.UI.SIZE,
			top: 5,
			left: 20,
			right: 20,
			bottom: 5,
			color: d.globalVals.palette.text,
		});
		$.textCont.add(text);
	} else if(items[i].type == 'button'){
		var btn = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: Ti.UI.SIZE,
			top: 5,
			left: 20,
			right: 20,
			bottom: 5,
			backgroundColor: d.globalVals.palette.primary,
			data: {type: 'button', linkTo: items[i].linkTo, link: items[i].link}
		});
		var label = Ti.UI.createLabel({
			height: Ti.UI.SIZE,
			top: 5,
			left: 10,
			right: 10,
			bottom: 5,
			textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
			color: d.globalVals.palette.background,
			font: {fontSize: 22},
			text: items[i].text,
			touchEnabled: false
		});
		
		btn.add(label);
		$.textCont.add(btn);
	}
}

//Add bottom pad
$.textCont.add(Ti.UI.createView({
	width: winprocs.screenWidth - 40,
	height: 10
}));
