var args = arguments[0] || {};
var winprocs = require('winprocs');
var procs = require('procs');
var d = Alloy.Globals.data;

$.fg.init({
    columns:3,
    space:5,
    gridBackgroundColor:'#fff',
    itemHeightDelta: 0,
    itemBackgroundColor:'#DDD',
    itemBorderColor:'transparent',
    itemBorderWidth:0,
    itemBorderRadius:0
});

var items = [];

var data = d.pages[args.id].imgs;

for (var i=0; i < data.length; i++){

    //CREATES A VIEW WITH OUR CUSTOM LAYOUT
    var view = Ti.UI.createView({});
    
    var img = Ti.UI.createImageView({
    	autoload: true,
    	image: data[i].image
    });
    //if(OS_ANDROID) procs.loadRemoteImage(img,data[i].image);
    
    view.add(img);
    
    //NOW WE PUSH TO THE ARRAY THE VIEW AND THE DATA
    items.push({
        view: view,
        data: {
	        title: data[i].title,
	        image: data[i].image
	    }
    });
};

//ADD ALL THE ITEMS TO THE GRID
$.fg.addGridItems(items);

$.fg.setOnItemClick(function(e){
    if(e.source.data.image){
		Alloy.Globals.imgview = e.source.data.image;
		winprocs.openWindow('photoView',{img: e.source.data.image},Alloy.Globals.nav);
	}
});