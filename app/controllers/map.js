var args = arguments[0] || {};
var Map = require('ti.map');

var d = Alloy.Globals.data.pages[args.id];

var pins = [];

for(var i = 0; i<d.pins.length; i++){
	var pin = Map.createAnnotation({
	    latitude:d.pins[i].lat,
	    longitude:d.pins[i].lon,
	    title:d.pins[i].title,
	    subtitle:d.pins[i].subtitle,
	    pincolor:Map.ANNOTATION_RED,
	    myid:i // Custom property to uniquely identify this annotation.
	});
	
	pins.push(pin);
}

var mapview = Map.createView({
    mapType: Map.NORMAL_TYPE,
    region: {latitude:d.location.lat, longitude:d.location.lon,
            latitudeDelta:d.location.delta, longitudeDelta:d.location.delta},
    animate:true,
    regionFit:true,
    userLocation:true,
    annotations:pins
});

$.mapCont.add(mapview);

mapview.addEventListener('click', function(evt) {
    Ti.API.info("Annotation " + evt.title + " clicked, id: " + evt.annotation.myid);
});