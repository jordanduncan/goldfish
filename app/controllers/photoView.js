var winprocs = require('winprocs');
var procs = require('procs');
var args = arguments[0] || {};

var win = $.photoWin;

var closeWin = function(){
	winprocs.closeWindow(win, true, Alloy.Globals.nav);
};

$.lineTop.setTransform(Ti.UI.create2DMatrix().rotate(-45));
$.lineBtm.setTransform(Ti.UI.create2DMatrix().rotate(45));

var img = Ti.UI.createImageView({
    image: Alloy.Globals.imgview,            
    width:Ti.UI.FILL
}); 
if(OS_ANDROID) procs.loadRemoteImage(img,Alloy.Globals.imgview);

$.image.add(img);

if(OS_ANDROID){
	var baseHeight, baseWidth;
	$.image.addEventListener('pinch', function(e) {
	    $.image.height = baseHeight * e.scale;
	    $.image.width = baseWidth * e.scale;
	    Ti.API.info(e.scale);
	});
	$.image.addEventListener('touchstart', function(e) {
	    baseHeight = $.image.height;
	    baseWidth = $.image.width;
	});
}
