var winprocs = require('winprocs');
var procs = require('procs');
var menuOpen = false;
var d = Alloy.Globals.data;
var mainView;

Alloy.Globals.nav = $.nav;

function openMenu(){
	if(menuOpen){
		$.main.animate({
			left: 0,
			duration: 250
		});
	} else {
		$.main.animate({
			left: 200,
			duration: 250
		});
	}
	menuOpen = !menuOpen;
}

function popMain(pageId){
	var type = d.pages[pageId].type;	
	$.content.removeAllChildren();
	
	if(OS_IOS){
		//Run memPool to clear old view from memory
		var pool = new winprocs.memPool();
		pool.clean(mainView);
	}
	
	mainView = Alloy.createController(type, {id: pageId}).getView();
	$.content.add(mainView);
	$.barTitle.setText(d.pages[pageId].title);
}

function popMenu(){
	var menuConts = [];
	
	for(var i in d.menu){
		var menuItem = Ti.UI.createTableViewRow({
			height: Ti.UI.SIZE,
			backgroundColor: d.globalVals.palette.primaryDark,
			selectedBackgroundColor: d.globalVals.palette.primary,
			data: {
			  title: d.menu[i].name,
			  type: d.pages[d.menu[i].id].type,
			  id: d.menu[i].id
			}
		});
		var title = Ti.UI.createLabel({
			height: Ti.UI.SIZE,
			text: d.menu[i].name,
			font: {fontSize: 18},
			color: d.globalVals.palette.background,
			top: 10,
			left: 15,
			bottom: 10
		});
		menuItem.add(title);
		menuConts.push(menuItem);
	}
	
	$.menuTable.setData(menuConts);
}

popMenu();

$.mainWin.addEventListener('open', function(){
	popMain(d.home);
});

$.menuTable.addEventListener('click', function(e){
	popMain(e.row.data.id);
	openMenu();
});

$.content.addEventListener('click', function(e){
	try {
		e.source.data.type;
	} catch(err) {
		return;
	}
	if(e.source.data.type == 'button' && e.source.data.linkTo){
		if(e.source.data.linkTo == 'page'){
			if(OS_IOS){
				$.content.animate({
					curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
					opacity:0,
					top: 30,
					duration:200
				});
			}
			setTimeout(function(){
				var type = d.pages[e.source.data.link].type;
				var view = Alloy.createController(type, {id: e.source.data.link}).getView();
				$.content.removeAllChildren();
				$.content.add(view);
				if(OS_IOS){
					$.content.animate({
						curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
						opacity:1,
						top: 0,
						duration:200
					});
				}
				$.barTitle.setText(d.pages[e.source.data.link].title);
			},200);
		} else if(e.source.data.linkTo == 'phone'){
			Ti.Platform.openURL('tel:'+e.source.data.link);
		} else if(e.source.data.linkTo == 'email'){
			Ti.Platform.openURL('mailto:'+e.source.data.link);
		}
	} else if(e.source.data.type == 'image' && e.source.data.src){
		Alloy.Globals.imgview = e.source.data.src;
		winprocs.openWindow('photoView',{img: e.source.data.src},Alloy.Globals.nav);
	}
});

if(d.globalVals.appVals.name){
	$.appName.setText(d.globalVals.appVals.name);
}
$.main.setWidth(winprocs.screenWidth);

Alloy.Globals.fa = $.fa;

if(OS_IOS) $.nav.open();
if(OS_ANDROID) $.mainWin.open();