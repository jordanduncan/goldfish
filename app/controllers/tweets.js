var winprocs = require('winprocs');
var procs = require('procs');
var moment = require('moment');

var args = arguments[0] || {};
var d = Alloy.Globals.data;

var Codebird = require("codebird");
var cb = new Codebird();
cb.setConsumerKey('AcE6RGrZVOgeMlFLthh3BsBjt', 'fPhOAC55pV4fuEO8W8H8vXdTQsNSAoZ5WYhIbbysRK0NHV486k');

var picSet = false;

//function to create tweet row
function addTweet(tweet){
	if(typeof tweet != 'object'){
		return false;
	}
	if(picSet == false){
		$.userImg.setImage(tweet.user.profile_image_url);
		//if(OS_ANDROID) procs.loadRemoteImage($.userImg, tweet.user.profile_image_url);
	}
	var row = Ti.UI.createTableViewRow({
		layout: 'vertical',
		height: Ti.UI.SIZE,
		backgroundColor: d.globalVals.palette.background,
		selectedBackgroundColor: d.globalVals.palette.primaryLight
	});

	var tweetLabel = Ti.UI.createLabel({
		top: 10,
		left: 20,
		right: 20,
		bottom: 10,
		height: Ti.UI.SIZE,
		color: d.globalVals.palette.text,
		text: tweet.text
	});
	row.add(tweetLabel);
	
	if(tweet.extended_entities != null){
		if(tweet.extended_entities.media != null){
			for(var i in tweet.extended_entities.media){
				Ti.API.info('image found:' + tweet.extended_entities.media[i].media_url);
				var img = Ti.UI.createImageView({
					width: Ti.UI.FILL,
					bottom: 10,
					left: 20,
					right: 20,
					data: {
						type: 'image',
				        src: tweet.extended_entities.media[i].media_url
				    }
				});
				img.setImage(tweet.extended_entities.media[i].media_url);
				//if(OS_ANDROID) procs.loadRemoteImage(img, tweet.extended_entities.media[i].media_url);
				row.add(img);
			}
		}
	}
	
	$.tweetTable.appendRow(row);
	
	var stamp = moment(tweet.created_at).fromNow();
	var dateLabel = Ti.UI.createLabel({
		top: 0,
		left: 20,
		right: 20,
		bottom: 10,
		height: Ti.UI.SIZE,
		color: d.globalVals.palette.primaryDark,
		font: {fontSize: 14},
		text: stamp
	});
	row.add(dateLabel);
	
}

//Get tweets from user 
var bearerToken = Ti.App.Properties.getString('TwitterBearerToken', null);
if(bearerToken == null){
	cb.__call(
	    'oauth2_token',
	    {},
	    function (reply) {
	        var bearer_token = reply.access_token;
	        cb.setBearerToken(bearer_token);
	        Ti.App.Properties.setString('TwitterBearerToken', bearer_token);
	        fetchTwitter();
	    }
	);
} else {
	Ti.API.info("We do have a bearer token...");
	cb.setBearerToken(bearerToken);
	fetchTwitter();
}
 
 
function fetchTwitter(){
	cb.__call(
	    'statuses/user_timeline',
	    "screen_name="+Ti.Network.encodeURIComponent(d.pages[args.id].account),
	    function (reply) {
	        for(var i in reply){
	        	Ti.API.info(JSON.stringify(reply[i]));
	        	addTweet(reply[i]);
	        }
	    },
	    true // this parameter required
	);
}

//Set user text
$.userLabel.setText('@'+d.pages[args.id].account);
