function isOnline(){
	if(Titanium.Network.networkType != Titanium.Network.NETWORK_NONE) return true;
	return false;
}

exports.isIOS7=function() {
  if (OS_IOS) {
    var version = Titanium.Platform.version.split(".");
    var major = parseInt(version[0],10);
    if (major >= 7) return true;
  }
  return false;
};

exports.loadRemoteImage = function (obj,url) {
  	var xhr = Titanium.Network.createHTTPClient();
    xhr.onload = function(){
		Ti.API.info('image data='+this.responseData);
		obj.image=this.responseData;
		obj.setHeight(Ti.UI.SIZE);
	};
	xhr.open('GET',url);
	xhr.send();
};