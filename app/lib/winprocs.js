var openWins = Alloy.Globals.openWins;

exports.memPool = function() {
    var _window;
    this.init = function() {
        _window = Ti.UI.createWindow();
        _window.hide();
        _window.open();
    };
    // This is where we clear out the memPool by closing it then reopening it again.
    this.clean = function(obj) {
        if(obj instanceof Array) {
            var arLen=obj.length;
            for ( var i=0, len=arLen; i<len; ++i ) {
                // We then stick the entire view into the pool
                _window.add(obj[i]);
            }
        } else {
            // We then stick the entire view into the pool
            _window.add(obj);
        }
        Ti.API.info('Cleaning MemoryPool.');
 
        // We empty the pool by closing it.
        _window.close();
 
        // We recreate the window again for the next object
        this.init();
    };
    this.init();
};

exports.openWindow =function(controller, winargs, navgroup) {  
  var win=Alloy.createController(controller, winargs).getView();
  if ((OS_IOS) && (!!navgroup)) navgroup.openWindow(win, {animated: true});
  if (OS_ANDROID) {
  	openWins.push(win);
    win.open();
    win.addEventListener('android:back', function (e) {
      e.cancelBubble = true;
      exports.closeWindow(win, true);
    });
    if(controller == 'home'){
    	Ti.App.fireEvent('homeFocused');
    }
  }
};

exports.closeWindow =function(win, animated, navgroup) { 
  if (animated===null) animated=true;
  if (OS_IOS) {
    if (!!navgroup) navgroup.closeWindow(win, {animated: animated});
  }
  //else win.close(animated?{animated: true, activityEnterAnimation: Ti.App.Android.R.anim.slide_in_left, activityExitAnimation: Ti.App.Android.R.anim.slide_out_right}:{});
  else win.close({});
  if(OS_ANDROID){
  	Ti.App.fireEvent('homeFocused');
  }
};

exports.addUpdater =function(win, event, callback) { 
  Ti.App.addEventListener(event, callback);
  if (!!win) win.addEventListener('close', function(){
    Ti.App.addEventListener(event, callback);  //This is required to allow the window object to be disposed of, tying up loose ends
  });
};

exports.addResumer =function(win, callback, callNow) { 
  if (OS_IOS) Ti.App.addEventListener('resume', callback);
  else win.addEventListener('open', function() {
    win.activity.addEventListener('resume', callback);
  });
  if (!!callNow) callback();
};

exports.addPauseResumer =function(win, pausecallback, resumecallback, callNow) { 
  if (OS_IOS) {
    Ti.App.addEventListener('paused', pausecallback);
    Ti.App.addEventListener('resume', resumecallback);  
  } else win.addEventListener('open', function() {
    win.activity.addEventListener('pause', pausecallback);
    win.activity.addEventListener('resume', resumecallback);      
  });
  if (!!callNow) resumecallback();
};

exports.info=function(what) {
  if (Ti.App.deployType=='development') Ti.API.info(what);
};

exports.debug=function(what) {
  if (Ti.App.deployType=='development') Ti.API.debug(what);
};
exports.warn=function(what) {
  if (Ti.App.deployType=='development') Ti.API.warn(what);
};

exports.error=function(what) {
  if (Ti.App.deployType=='development') Ti.API.error(what);
};

exports.log=function(what) {
  if (Ti.App.deployType=='development') Ti.API.log(what);
};

exports.trace=function(what) {
  if (Ti.App.deployType=='development') Ti.API.trace(what);
};

exports.density=OS_ANDROID?Ti.Platform.displayCaps.logicalDensityFactor:1;
exports.screenWidth=Ti.Platform.displayCaps.platformWidth/exports.density;
exports.screenHeight=Ti.Platform.displayCaps.platformHeight/exports.density;

Ti.Gesture.addEventListener('orientationchange', function(e) {
	exports.screenWidth=Ti.Platform.displayCaps.platformWidth/exports.density;
	exports.screenHeight=Ti.Platform.displayCaps.platformHeight/exports.density;
});


