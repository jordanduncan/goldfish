// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Alloy.Globals.data = {
  "globalVals": {
    "palette":{
      "primary": "#33AF90",
      "primaryDark": "#1F6A57",
      "primaryLight": "#8DE8D1",
      "background": "#EEEEEE",
      "textPrimary": "#33AF90",
      "text": "#2C2C2C"
    },
    "appVals":{
      "id": "1",
      "name": "Test Data App",
      "desc": "this is the description for the app that will appear somewhere.",

    }
  },
  "home": "1",
  "menu":[
    {
      "id":"1",
      "name":"About Us",
      "icon":"home"
    },{
      "id":"2",
      "name":"Gallery",
      "icon":"gallery"
    },{
      "id":"3",
      "name":"Map",
      "icon":"map"
    },{
      "id":"4",
      "name":"Tweets",
      "icon":"twitter"
    },{
      "id":"5",
      "name":"Contact",
      "icon":"phone"
    }
  ],
  "pages":{
    "1":{
      "title": "About Us",
      "type": "text",
      "items": [
        {
          "type": "image",
          "src": "http://lorempixel.com/600/400/",
          "hwRatio": 0.67
        },{
          "type": "heading",
          "text": "Lorem Ipsum"
        },{
          "type": "text",
          "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean hendrerit eget sapien vel hendrerit. Ut condimentum, quam fermentum vulputate ultrices, tellus ipsum gravida libero, placerat facilisis urna felis quis leo. Cras ipsum tellus, pellentesque quis cursus in, dictum ut erat. Donec orci odio, fermentum sit amet massa eu, dictum ultrices ipsum. Quisque vehicula nisl nibh, in tempus eros ultricies eget. Nulla convallis sapien massa, ut consectetur dolor mollis nec. Proin sit amet est quam. In malesuada mattis tellus, ac viverra ante lobortis sit amet. Vivamus non velit euismod, accumsan sem quis, dignissim erat. Etiam lacinia arcu neque, a bibendum ipsum mattis ac. Vestibulum id ante massa. Nam id diam ligula."
        },{
          "type": "button",
          "text": "Link",
          "linkTo": "page",
          "link": "2"
        }
      ]
    },
    "2":{
      "title": "Gallery",
      "type": "gallery",
      "imgs": [
        {title:'sample 1', image:'http://dummyimage.com/600x400/000/00ffd5.png'},
        {title:'sample 2', image:'http://dummyimage.com/601x201/000/00ffd5.png'},
        {title:'sample 3', image:'http://dummyimage.com/400x600/000/00ffd5.png'},
        {title:'sample 4', image:'http://dummyimage.com/200x400/000/00ffd5.png'},
        {title:'sample 5', image:'http://dummyimage.com/630x400/000/00ffd5.png'},
        {title:'sample 6', image:'http://dummyimage.com/600x420/000/00ffd5.png'},
        {title:'sample 7', image:'http://dummyimage.com/650x400/000/00ffd5.png'},
        {title:'sample 8', image:'http://dummyimage.com/600x400/000/00ffd5.png'},
        {title:'sample 9', image:'http://dummyimage.com/680x400/000/00ffd5.png'},
        {title:'sample 9', image:'http://dummyimage.com/300x300/000/00ffd5.png'},
        {title:'sample 9', image:'http://dummyimage.com/600x400/000/00ffd5.png'},
        {title:'sample 9', image:'http://dummyimage.com/400x500/000/00ffd5.png'},
        {title:'sample 9', image:'http://dummyimage.com/100x400/000/00ffd5.png'},
        {title:'sample 9', image:'http://dummyimage.com/300x400/000/00ffd5.png'},
        {title:'sample 9', image:'http://dummyimage.com/400x400/000/00ffd5.png'}
      ]
    },
    "3":{
      "title": "Map",
      "type": "map",
      "location":{
      	"lat": 37.390749,
      	"lon": -122.081651,
      	"delta": 0.01
      },
      "pins": [
      	{
      	  "title": "Title",
      	  "subtitle": "subtitle",
      	  "lat": 37.390749,
      	  "lon": -122.081651
      	},{
      	  "title": "Title 2",
      	  "subtitle": "subtitle2",
      	  "lat": 39.390749,
      	  "lon": -115.081651
      	}
      ]
    },
    "4":{
      "title": "Tweets",
      "type": "tweets",
      "account": "JrdnDncn"
    },
    "5":{
      "title": "Contact",
      "type": "contact",
      "items": [
      	{
          "type": "phone",
          "number": "07741778891"
        },{
          "type": "email",
          "address": "jordan@dogfi.sh"
        },{
          "type": "phone",
          "number": "07741778891"
        }
      ]
    }
  }
};

Alloy.Globals.openWins = [];
